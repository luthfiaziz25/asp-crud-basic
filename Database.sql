USE [belajarcrud]
GO
/****** Object:  Table [dbo].[asal]    Script Date: 27/07/2021 16:24:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[asal](
	[asal_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[nama_asal] [varchar](50) NULL,
	[tanggal_buat] [datetime] NULL,
	[tanggal_edit] [datetime] NULL,
	[dibuat_oleh] [varchar](50) NULL,
	[diedit_oleh] [varchar](50) NULL,
	[status] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[jenis]    Script Date: 27/07/2021 16:24:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[jenis](
	[jenis_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[nama_jenis] [varchar](50) NULL,
	[tanggal_buat] [datetime] NULL,
	[tanggal_edit] [datetime] NULL,
	[dibuat_oleh] [varchar](50) NULL,
	[diedit_oleh] [varchar](50) NULL,
	[status] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[makhluk_hidup]    Script Date: 27/07/2021 16:24:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[makhluk_hidup](
	[mk_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[nama_mk] [varchar](255) NULL,
	[tempat_lahir] [varchar](50) NULL,
	[tanggal_lahir] [datetime] NULL,
	[jenis_id] [numeric](18, 0) NULL,
	[asal_id] [numeric](18, 0) NULL,
	[tanggal_buat] [datetime] NULL,
	[tanggal_edit] [datetime] NULL,
	[dibuat_oleh] [varchar](50) NULL,
	[diedit_oleh] [varchar](50) NULL,
	[status] [bit] NULL
) ON [PRIMARY]
GO
