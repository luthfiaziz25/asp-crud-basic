﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;
using pembelajaran_crud.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace pembelajaran_crud.Component
{
    public class Helper
    {
        SqlConnection con = new SqlConnection();
        SqlCommand com = new SqlCommand();
        SqlDataReader dr;
        private static string connectionString = "Data Source=LAPTOP-ICS01MKG;Initial Catalog=belajarcrud;Integrated Security=True";

        public static List<SelectListItem> DropDownAsal()
        {

            List<SelectListItem> asalList = new List<SelectListItem>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT asal_id, nama_asal FROM asal WHERE status=1", con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    asalList.Add(new SelectListItem
                    {
                        Text = (string)dr["nama_asal"],
                        Value = (string)dr["asal_id"].ToString()
                    });
                }

                con.Close();
            }
            return asalList;
        }


        public static List<SelectListItem> DropDownJenis()
        {

            List<SelectListItem> jenisList = new List<SelectListItem>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT jenis_id, nama_jenis FROM jenis WHERE status=1", con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    jenisList.Add(new SelectListItem
                    {
                        Text = (string)dr["nama_jenis"],
                        Value = (string)dr["jenis_id"].ToString()
                    });
                }

                con.Close();
            }
            return jenisList;
        }
    }
}
