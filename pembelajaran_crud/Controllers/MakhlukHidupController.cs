﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using pembelajaran_crud.Data;
using pembelajaran_crud.Models;

namespace pembelajaran_crud.Controllers
{
    public class MakhlukHidupController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MakhlukHidupController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadDataMakhluk()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                MakhlukHidup makhluk = new MakhlukHidup(_context);
                var datamakhluk = makhluk.getDataMakhluk();


                if (!string.IsNullOrEmpty(searchValue))
                {
                    datamakhluk = datamakhluk.Where(m =>
                    m.nama_mk.ToLower().Contains(searchValue.ToLower()) ||
                    m.nama_asal.ToString().Contains(searchValue.ToLower())
                    );
                }
                recordsTotal = datamakhluk.Count();

                var data = datamakhluk.Skip(skip).Take(pageSize).ToList();

                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult PartialForm(string? action,string? id)
        {
            ViewBag.id = "";
            ViewBag.nama = "";
            ViewBag.tempatlahir = "";
            ViewBag.tanggal = "";
            ViewBag.jenis = "";
            ViewBag.asal = "";

            if (action == "edit")
            {
                var data = _context.makhluk_hidup.Where(x => x.mk_id.Equals(decimal.Parse(id)));
                foreach (var item in data)
                {
                    ViewBag.id = item.mk_id;
                    ViewBag.nama = item.nama_mk;
                    ViewBag.tempatlahir = item.tempat_lahir;
                    ViewBag.tanggal = item.tanggal_lahir.ToString("yyyy-MM-dd");
                    ViewBag.jenis = item.jenis_id;
                    ViewBag.asal = item.asal_id;
                }
            }
            return PartialView();
        }

        public IActionResult InsertUpdateData()
        {
            var id = Request.Form["id"];
            var action = "";
            var formdata = new MakhlukHidup.FormMakhluk();
            MakhlukHidup makhluk = new MakhlukHidup(_context);

            formdata.nama_mk = Request.Form["nama"];
            formdata.tempat_lahir = Request.Form["tempat"];
            formdata.tanggal_lahir = Request.Form["tanggal"];
            formdata.asal_id = Request.Form["asal"];
            formdata.jenis_id = Request.Form["jenis"];

            if (id != "")
            {
                action = "Diedit";
                formdata.mk_id = Request.Form["id"];
                makhluk.UpdateMakhluk(formdata);
            }
            else
            {
                action = "Ditambahkan";
                makhluk.InsertMakhluk(formdata);
            }
            return Ok("Data Berhasil "+ action);
        }

        public IActionResult DeleteData(string? id)
        {
            try
            {
                var _id = id;
                MakhlukHidup makhluk = new MakhlukHidup(_context);
                MakhlukHidup.FormMakhluk form = new MakhlukHidup.FormMakhluk();

                form.mk_id = _id;
                makhluk.DeleteMakhluk(form);

                return Ok("Data Berhasil Di Hapus!");
            }
            catch(DataException)
            {
                ModelState.AddModelError("", "Unable to Delete. Try again, and if the problem persists see your system administrator.");
                return Ok();
            }
        }

    }
}
