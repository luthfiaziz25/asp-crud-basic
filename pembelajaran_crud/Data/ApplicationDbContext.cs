﻿using Microsoft.EntityFrameworkCore;
using pembelajaran_crud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pembelajaran_crud.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<MakhlukHidup> makhluk_hidup { get; set; }
    }
}
