﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using pembelajaran_crud.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pembelajaran_crud.Models
{
    public class MakhlukHidup
    {
        private readonly ApplicationDbContext _context;
        public MakhlukHidup(ApplicationDbContext context)
        {
            _context = context;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal mk_id { get; set; }
        public string nama_mk { get; set; }
        public string tempat_lahir { get; set; }
        public DateTime tanggal_lahir { get; set; }
        public decimal jenis_id { get; set; }
        public decimal asal_id { get; set; }
        public DateTime tanggal_buat { get; set; }
        public DateTime? tanggal_edit { get; set; }
        public string dibuat_oleh { get; set; }
        public string diedit_oleh { get; set; }
        public bool status { get; set; }

        public class ViewDataMakhluk
        {
            public int no { get; set; }
            public string mk_id { get; set; }
            public string nama_mk { get; set; }
            public string tempat_lahir { get; set; }
            public string tanggal_lahir { get; set; }
            public string nama_jenis { get; set; }
            public string nama_asal { get; set; }
        }

        public class FormMakhluk
        {
            public string mk_id { get; set; }
            public string nama_mk { get; set; }
            public string tempat_lahir { get; set; }
            public string tanggal_lahir { get; set; }
            public string jenis_id { get; set; }
            public string asal_id { get; set; }
        }

        public IEnumerable<ViewDataMakhluk> getDataMakhluk()
        {
            List<ViewDataMakhluk> listmk = new List<ViewDataMakhluk>();
            var connectionString = _context.Database.GetDbConnection().ConnectionString;
            string query = "SELECT a.mk_id,a.nama_mk,a.tempat_lahir,CONVERT(VARCHAR(10), a.tanggal_lahir, 105) as tgl_lahir," +
                           "b.nama_jenis,c.nama_asal FROM makhluk_hidup a " +
                           "LEFT JOIN jenis b ON b.jenis_id = a.jenis_id " +
                           "LEFT JOIN asal c ON c.asal_id = a.asal_id " +
                           "WHERE a.status = 1";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(
                query, connection);
                connection.Open();
                int i = 1;
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ViewDataMakhluk makhluk = new ViewDataMakhluk();
                        makhluk.no = i;
                        makhluk.mk_id = reader[0].ToString();
                        makhluk.nama_mk = reader[1].ToString();
                        makhluk.tempat_lahir = reader[2].ToString();
                        makhluk.tanggal_lahir = reader[3].ToString();
                        makhluk.nama_jenis = reader[4].ToString();
                        makhluk.nama_asal = reader[5].ToString();
                        listmk.Add(makhluk);
                        i++;
                    }
                }
                connection.Close();
            }
            return listmk;
        }

        public void DeleteMakhluk(FormMakhluk fmakhluk)
        {
            var query = _context.makhluk_hidup.Where(x => x.mk_id.Equals(decimal.Parse(fmakhluk.mk_id)));
            foreach (var item in query)
            {
                item.status = false;
                item.tanggal_edit = DateTime.Now;
                item.diedit_oleh = "admin";
            }
            _context.SaveChanges();
        }

        public void UpdateMakhluk(FormMakhluk fmakhluk)
        {
            var query = _context.makhluk_hidup.Where(x => x.mk_id.Equals(decimal.Parse(fmakhluk.mk_id)));
            foreach (var item in query)
            {
                item.tanggal_edit = DateTime.Now;
                item.diedit_oleh = "admin";
                item.jenis_id = decimal.Parse(fmakhluk.jenis_id);
                item.asal_id = decimal.Parse(fmakhluk.asal_id);
                item.tanggal_lahir = DateTime.Parse(fmakhluk.tanggal_lahir);
                item.tempat_lahir = fmakhluk.tempat_lahir;
                item.nama_mk = fmakhluk.nama_mk;
            }
            _context.SaveChanges();
        }

        public void InsertMakhluk(FormMakhluk fmakhluk)
        {
            var datamakhluk = new MakhlukHidup(_context)
            {
                nama_mk = fmakhluk.nama_mk,
                tempat_lahir = fmakhluk.tempat_lahir,
                tanggal_lahir = DateTime.Parse(fmakhluk.tanggal_lahir),
                jenis_id = decimal.Parse(fmakhluk.jenis_id),
                asal_id = decimal.Parse(fmakhluk.asal_id),
                tanggal_buat = DateTime.Now,
                dibuat_oleh = "admin",
                status = true
            };
            _context.makhluk_hidup.Add(datamakhluk);
            _context.SaveChanges();
        }
    }
}
